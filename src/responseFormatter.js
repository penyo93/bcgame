/*
feloldások:
    hcp = handicap,
        1714 = hazai (maybe),
        1715 = vendég (maybe),
    total = total :),
        12: hazai (maybe),
        13: vendég (maybe)
     querternr={number} = hánadik negyed
     score= score,
 */

//https://stackoverflow.com/questions/8217419/how-to-determine-if-javascript-array-contains-an-object-with-an-attribute-that-e

import {arrayEquals, updateObjects} from "./updateObjects.js";
import {arrayGenerator} from "./arrayGenerator.js";

let values = new Map();

const responseFormatter = (response) => {
    if (values.has(response[0][0])) {
        if (response[0][2] !== undefined) {
            switchValues(response[0][1], response[0][2], response[1])
        } else {
            switchValues(response[0][1], "", response[1])
        }
    } else {
        values.set(response[0][0], [{
            'desc': [response[1]]
        }])
    }
}

const switchValues = (type, marketID, value) => {
    values.forEach((value1, key, map) => {
        if (values.get(key)[type] === undefined || (marketID !== undefined && values.get(key)[type][marketID] === undefined)) {
            if (type === 'market') {
                if (map.get(key)['market'] === undefined) {
                    map.get(key)['market'] = [];
                }
                map.get(key)['market'][marketID] = [value]
            } else {
                map.get(key)[type] = [value];
            }
        } else {
            if (value !== null) {
                if (type === 'market') {
                    if (map.get(key)[type][marketID].filter(it => arrayEquals(Object.keys(it), Object.keys(value))).length === 0) {
                        map.get(key)[type][marketID].push(value);
                    }
                } else {
                    if (map.get(key)[type].filter(it => arrayEquals(Object.keys(it), Object.keys(value))).length === 0) {
                        map.get(key)[type].push(value);
                    }
                }

                values = updateObjects(key, value, type, values,marketID === undefined ? "" : marketID)
            }
        }
    })
    arrayGenerator(values)
}
export {responseFormatter}