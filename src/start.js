import puppeteer from 'puppeteer-extra'
import {responseFormatter} from "./responseFormatter.js";
import {getBasketBall_ID} from "./getBasketBall_ID.js";
import StealthPlugin from 'puppeteer-extra-plugin-stealth'


(async () => {
    puppeteer.use(StealthPlugin())
    await puppeteer
        .launch({
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            const basketBallIds = new Set();
            page.on("response", async (response) => {
                if (response.url().startsWith("https://api1.sptpub.com/api/v1/live/brand/")) {
                    await response
                        .json()
                        .then(value => {
                            value.items.forEach(match => {
                                if (match.length !== 0) {
                                    let baskBallId = getBasketBall_ID(match)
                                    baskBallId !== "" && basketBallIds.add(baskBallId)
                                    if (basketBallIds.has(match[0][0])) {
                                        responseFormatter(match)
                                    }
                                }
                            })

                        })
                        .catch(err => console.log(err))
                }
            });

            await page.goto("https://bc.game/sports", {
                waitUntil: "load",
                timeout: 0,
            });
        });
})()



