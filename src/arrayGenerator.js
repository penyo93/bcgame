let marketArray = [];

const arrayGenerator = (values) => {
    values.forEach((value, key) => {
        let obj = values.get(key)
        if (obj['market'] !== undefined) {
            let odds = [];
            obj['market'].filter(it => it.length !== 0).forEach(it => {
                let marketId = obj['market'].indexOf(it);
                if (Object.keys(it[0])[0].startsWith("hcp")) {
                    Object.keys(it[0]).forEach((obKey, index) => {
                        let homeObj = {
                            [obKey]: `${Object.values(it[0])[index][1714]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[0]}`
                        };
                        let awayObj = {
                            [obKey]: `${Object.values(it[0])[index][1715]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[1]}`
                        };
                        odds.push({
                            "home": homeObj,
                            "away": awayObj
                        })

                    })
                } else if (Object.keys(it[0])[0].startsWith("total")) {
                    Object.keys(it[0]).forEach((obKey, index) => {
                        let homeObj = {
                            [obKey]: `${Object.values(it[0])[index][12]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[0]}`
                        };
                        let awayObj = {
                            [obKey]: `${Object.values(it[0])[index][13]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[1]}`
                        };
                        odds.push({
                            "home": homeObj,
                            "away": awayObj
                        })
                    })
                } else if (Object.keys(it[0])[0] === "") {
                    let homeObj = {
                        "home_win": `${Object.values(it[0])[0][4]['k']}`,
                        "home_win_market_id": `${marketId}`,
                        "home_win_outcome_id": `${Object.keys(Object.values(it[0])[0])[0]}`
                    };
                    let awayObj = {
                        "away_win": `${Object.values(it[0])[0][5]['k']}`,
                        "away_win_market_id": `${marketId}`,
                        "away_win_outcome_id": `${Object.keys(Object.values(it[0])[0])[1]}`
                    };
                    odds.push({
                        "home": homeObj,
                        "away": awayObj
                    })
                } else if (Object.keys(it[0])[0].startsWith("quarter")) {
                    Object.keys(it[0]).forEach((obKey, index) => {
                        let homeObj = {
                            [obKey]: `${Object.values(it[0])[index][70]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[0]}`
                        };
                        let awayObj = {
                            [obKey]: `${Object.values(it[0])[index][72]['k']}`,
                            [obKey.concat("_market_id")]: `${marketId}`,
                            [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[1]}`
                        };
                        odds.push({
                            "home": homeObj,
                            "away": awayObj
                        })
                    })
                } else if (Object.keys(it[0])[0].includes("|")) {
                    if (Object.keys(it[0])[0].split[0]("|")[1].startsWith("hcp")) {
                        Object.keys(it[0]).forEach((obKey, index) => {
                            let homeObj = {
                                [obKey]: `${Object.values(it[0])[index][1714]['k']}`,
                                [obKey.concat("_market_id")]: `${marketId}`,
                                [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[0]}`
                            };
                            let awayObj = {
                                [obKey]: `${Object.values(it[0])[index][1715]['k']}`,
                                [obKey.concat("_market_id")]: `${marketId}`,
                                [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[1]}`
                            };
                            odds.push({
                                "home": homeObj,
                                "away": awayObj
                            })
                        })
                    } else if (Object.keys(it[0])[0].split[0]("|")[1].startsWith("total")) {
                        Object.keys(it[0]).forEach((obKey, index) => {
                            let homeObj = {
                                [obKey]: `${Object.values(it[0])[index][12]['k']}`,
                                [obKey.concat("_market_id")]: `${marketId}`,
                                [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[0]}`
                            };
                            let awayObj = {
                                [obKey]: `${Object.values(it[0])[index][13]['k']}`,
                                [obKey.concat("_market_id")]: `${marketId}`,
                                [obKey.concat("_outcome_id")]: `${Object.keys(Object.values(it[0])[index])[1]}`
                            };
                            odds.push({
                                "home": homeObj,
                                "away": awayObj
                            })
                        })
                    }
                }
            })
            let json =
                {
                    "leagueName": `${obj[0]['desc'][0]['tournament']['name']}`,
                    "teams": `${obj[0]['desc'][0]['competitors'][0]['name']} - ${obj[0]['desc'][0]['competitors'][1]['name']}`,
                    "score": obj['score'] === undefined ? '' : `${Object.keys(obj['score'][0])[0]}: ${Object.values(obj['score'][0])[0]}, ${Object.keys(obj['score'][0])[1]} : ${Object.values(obj['score'][0])[1]}`,
                    "matchTime": obj['state'] === undefined ? '' : obj['state'][0]['clock'] === undefined ? '' : `${obj['state'][0]['clock']['match_time']}`,
                    "remainingTime": obj['state'] === undefined ? '' : obj['state'][0]['clock'] === undefined ? '' : `${obj['state'][0]['clock']['remaining_time']}`,
                    "status": obj['state'] === undefined ? '' : `${obj['state'][0]['status']} quartet`,
                    "odds": odds
                };
            if (marketArray.length > 0 && marketArray.some(it => Object.keys(it).some(innerKey => innerKey === key))) {
                let index = marketArray.findIndex(it =>  Object.keys(it)[0] === key);
                marketArray[index] = {[key]: json};
            } else {
                marketArray.push({[key]: json})
            }
        }
    })
}


const getOdds = () => {
    return marketArray;
}

export {arrayGenerator, getOdds}