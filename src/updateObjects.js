const updateObjects = (key, value, type, values, marketID) => {
    if (type !== 'market') {
        values.get(key)[type].forEach((item, index) => {
            if (arrayEquals(Object.keys(item), Object.keys(value))) {
                values.get(key)[type][index] = value
            }
        })
    }
    else {
        values.get(key)[type][marketID].forEach((item, index) => {
            if (arrayEquals(Object.keys(item), Object.keys(value))) {
                values.get(key)[type][marketID][index] = value
            }
        })
    }
    return values
}

function arrayEquals(a, b) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

export {updateObjects, arrayEquals}